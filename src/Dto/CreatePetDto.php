<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contains and validates data for creating a new Pet.
 */
readonly class CreatePetDto
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Choice(['cat', 'dog'])]
        public string $species,
        #[Assert\Type('string')]
        #[Assert\Length(min: 3, max: 64)]
        public string $name,
        #[Assert\Type('string')]
        #[Assert\Length(min: 3, max: 64)]
        public string $breed,
        #[Assert\NotBlank]
        #[Assert\Choice(['female', 'male'])]
        public string $gender,
    ) {
    }
}
