<?php

namespace App\Controller;

use App\Dto\CreatePetDto;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Controller which handles various actions surrounding Pet management.
 */
class PetController extends AbstractController
{
    public function __construct(
        private HttpClientInterface $client,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * Fetches and normalizes a list of breeds matching a given search value.
     */
    #[Route('/api/breeds/{species}/search/{search}', name: 'api_pet_breeds', methods: ['GET', 'HEAD'])]
    public function breeds(string $species, string $search): JsonResponse
    {
        switch ($species) {
            case 'dog':
                try {
                    $response = $this->client->request('GET', "https://api.algobook.info/v1/dogs/search/{$search}");

                    return $this->json(['data' => array_map(fn (array $dog) => $dog['name'], $response->toArray())]);
                } catch (\Throwable $e) {
                    $this->logger->error('Unable to load dog breeds from API', ['exception' => $e]);

                    return $this->json(['error' => 'Unable to load dog breeds.']);
                }

            case 'cat':
                try {
                    $response = $this->client->request('GET', 'https://api.thecatapi.com/v1/breeds');
                    $filteredData = [];
                    foreach ($response->toArray() as $breedData) {
                        $name = $breedData['name'];
                        if (false !== stripos($name, $search)) {
                            $filteredData[] = $name;
                        }
                    }

                    return $this->json(['data' => $filteredData]);
                } catch (\Throwable $e) {
                    $this->logger->error('Unable to load dog breeds from API', ['exception' => $e]);

                    return $this->json(['error' => 'Unable to load dog breeds.']);
                }
        }

        return $this->json(['error' => 'Unsupported species']);
    }

    /**
     * Handles creation of a new Pet.
     */
    #[Route('/api/pet', name: 'pet_create', methods: ['POST'])]
    public function create(#[MapRequestPayload] CreatePetDto $dto): JsonResponse
    {
        return $this->json(['data' => [
            'species' => $dto->species,
            'breed' => $dto->breed,
            'name' => $dto->name,
            'gender' => $dto->gender,
        ]], 201);
    }
}
