FROM webdevops/php-nginx-dev:8.2-alpine
ENV WEB_DOCUMENT_ROOT=/app/public
WORKDIR /app
RUN chown -R www-data:www-data .
USER www-data

COPY assets assets
COPY bin bin
COPY config config
COPY public public
COPY src src
COPY templates templates
COPY tests tests
RUN mkdir -p var/cache var/log
COPY .env* ./
COPY composer.* ./
COPY importmap.php importmap.php
COPY phpunit.xml.dist phpunit.xml.dist
COPY symfony.lock symfony.lock

RUN composer install

EXPOSE 80
