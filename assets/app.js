import './styles/app.css';

import {createApp} from 'vue';
import {createRouter, createWebHashHistory} from 'vue-router';
import App from '@/App.vue';

// Vuetify
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {path: '/', name: 'home', component: () => import('@/Home.vue')},
        {path: '/pet/:species', name: 'pet_form', props: true, component: () => import('@/PetForm.vue')},
    ],
})

const vuetify = createVuetify({
    components,
    directives,
})

createApp(App)
    .use(router)
    .use(vuetify)
    .mount('#app')
