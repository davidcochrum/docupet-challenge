<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PetControllerTest extends WebTestCase
{
    public function testCreateWhenValid(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/pet', [
            'species' => 'dog',
            'breed' => 'American Staffordshire Terrier',
            'name' => 'Maximillionare Bubba Cochrum',
            'gender' => 'male',
        ]);

        $this->assertResponseStatusCodeSame(201);
        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $json = json_decode($response, true);
        $this->assertEquals([
            'data' => [
                'species' => 'dog',
                'breed' => 'American Staffordshire Terrier',
                'name' => 'Maximillionare Bubba Cochrum',
                'gender' => 'male',
            ],
        ], $json, print_r($json, true));
    }

    /** @dataProvider createWhenInvalidData */
    public function testCreateWhenInvalid(array $data, string $expectedDetail): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/pet', $data);

        $this->assertResponseIsUnprocessable();
        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $json = json_decode($response, true);
        $this->assertEquals($expectedDetail, $json['detail'] ?? null, print_r($json, true));
    }

    public function createWhenInvalidData(): array
    {
        return [
            'bad species' => [
                'data' => [
                    'species' => 'cat',
                    'name' => 'Monte',
                    'breed' => 'Orange',
                    'gender' => 'male',
                ],
                'expectedDetail' => 'species: The value you selected is not a valid choice.',
            ],
            'name too short' => [
                'data' => [
                    'species' => 'dog',
                    'name' => 'Mo',
                    'breed' => 'Beagle',
                    'gender' => 'male',
                ],
                'expectedDetail' => 'name: This value is too short. It should have 3 characters or more.',
            ],
            'name too long' => [
                'data' => [
                    'species' => 'dog',
                    'name' => str_repeat('A', 65),
                    'breed' => 'Beagle',
                    'gender' => 'male',
                ],
                'expectedDetail' => 'name: This value is too long. It should have 64 characters or less.',
            ],
            'breed too short' => [
                'data' => [
                    'species' => 'dog',
                    'name' => 'Monte',
                    'breed' => 'Be',
                    'gender' => 'male',
                ],
                'expectedDetail' => 'breed: This value is too short. It should have 3 characters or more.',
            ],
            'breed too long' => [
                'data' => [
                    'species' => 'dog',
                    'name' => 'Monte',
                    'breed' => str_repeat('A', 65),
                    'gender' => 'male',
                ],
                'expectedDetail' => 'breed: This value is too long. It should have 64 characters or less.',
            ],
            'bad gender' => [
                'data' => [
                    'species' => 'dog',
                    'name' => 'Monte',
                    'breed' => 'Gold',
                    'gender' => 'fish',
                ],
                'expectedDetail' => 'gender: The value you selected is not a valid choice.',
            ],
            'combo' => [
                'data' => [
                    'species' => 'fish',
                    'name' => 'Monte',
                    'breed' => 'Gold',
                    'gender' => 'fish',
                ],
                'expectedDetail' => "species: The value you selected is not a valid choice.\ngender: The value you selected is not a valid choice.",
            ],
        ];
    }
}
