# David Cochrum's DocuPet Challenge

I was given the task of creating a simple full-stack app for registering a pet.
The complete spec sheet can be accessed here: [DocuPet - Full Stack Coding Test.pdf](/DocuPet%20-%20Full%20Stack%20Coding%20Test.pdf)

I trust you'll review the app and be left with 😍 and 🤩.

## 🏃 Running The App

### Requirements
- PHP 8.2 (taking advantage of some new features):
  - Install locally with [Brew](https://brew.sh):
    ```shell
    brew tap shivammathur/php
    brew install shivammathur/php/php@8.2
    ```
    And the [Symfony CLI](https://symfony.com/download):
    ```shell
    brew install symfony-cli/tap/symfony-cli
    ```
  - Or install [Docker](https://www.docker.com/products/docker-desktop/)
    and let the [Dockerfile](/Dockerfile) do all the work.
- NodeJS v16+
  - Install locally with [Brew](https://brew.sh):
    ```shell
    brew install node
    ```

### Startup
Start the webpack server to build and watch the frontend Vue files:
```shell
yarn install
yarn run build
```

#### Local PHP
Install PHP dependencies and start serving
```shell
composer install
symfony server:start
```

#### Docker PHP

Start the Docker PHP server:
```shell
docker run --rm -it -p 8000:443 $(docker build -q .)
```

### Access The App
To access the application, open [localhost:8000](https://localhost:8000) in your browser.

## ✅ Running Tests

### Local PHP
```shell
vendor/bin/phpunit
```

### Docker PHP
```shell
docker run --rm $(docker build -q .) vendor/bin/phpunit
```
